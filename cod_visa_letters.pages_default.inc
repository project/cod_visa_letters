<?php
/**
 * @file
 * cod_visa_letters.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cod_visa_letters_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'cod_visa_letters_page';
  $page->task = 'page';
  $page->admin_title = 'Visa Letters';
  $page->admin_description = '';
  $page->path = 'admin/conference/%node/tickets/visa-letters/!bundle';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'og_is_node_group',
        'settings' => NULL,
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'og_perm',
        'settings' => array(
          'perm' => 'administer group',
        ),
        'context' => array(
          0 => 'logged-in-user',
          1 => 'argument_entity_id:node_1',
        ),
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'tab',
    'title' => 'Visa Letters',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'node' => array(
      'id' => 1,
      'identifier' => 'Conference',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
    'bundle' => array(
      'id' => 1,
      'identifier' => 'Bundle',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 0,
      ),
    ),
  );
  $page->conf = array(
    'admin_paths' => TRUE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_cod_visa_letters_page__panel_context_070fcd4c-2a34-4aa4-8c7b-abb539d532ec';
  $handler->task = 'page';
  $handler->subtask = 'cod_visa_letters_page';
  $handler->handler = 'panel_context';
  $handler->weight = -30;
  $handler->conf = array(
    'title' => 'Visa Listing',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'Webform from cod_visa_letter',
        'keyword' => 'cod_visa_letter_webform',
        'name' => 'webform_from_cod_visa_letter',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'vertical_tabs',
          1 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'vertical_tabs' => array(
        'type' => 'region',
        'title' => 'Vertical Tabs',
        'width' => '240',
        'width_type' => 'px',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'vertical_tabs' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Listing page';
  $display->uuid = '083a2221-88e6-4ce6-99b6-e02c4029f6a8';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_cod_visa_letters_page__panel_context_070fcd4c-2a34-4aa4-8c7b-abb539d532ec';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-26f2a0e6-413c-4a14-b216-ae9c15bf1066';
  $pane->panel = 'center';
  $pane->type = 'views_panes';
  $pane->subtype = 'cod_visa_letter_listing-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '26f2a0e6-413c-4a14-b216-ae9c15bf1066';
  $display->content['new-26f2a0e6-413c-4a14-b216-ae9c15bf1066'] = $pane;
  $display->panels['center'][0] = 'new-26f2a0e6-413c-4a14-b216-ae9c15bf1066';
  $pane = new stdClass();
  $pane->pid = 'new-a9ebe133-77c8-4c4c-a27a-1bf490c1fd30';
  $pane->panel = 'vertical_tabs';
  $pane->type = 'cod_visa_letters_vertical_tabs_pane';
  $pane->subtype = 'cod_visa_letters_vertical_tabs_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a9ebe133-77c8-4c4c-a27a-1bf490c1fd30';
  $display->content['new-a9ebe133-77c8-4c4c-a27a-1bf490c1fd30'] = $pane;
  $display->panels['vertical_tabs'][0] = 'new-a9ebe133-77c8-4c4c-a27a-1bf490c1fd30';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-26f2a0e6-413c-4a14-b216-ae9c15bf1066';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_cod_visa_letters_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'cod_visa_letters_page';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'vertical_tabs',
          1 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'vertical_tabs' => array(
        'type' => 'region',
        'title' => 'Vertical Tabs',
        'width' => '240',
        'width_type' => 'px',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'preface' => NULL,
      'content' => NULL,
      'footer' => NULL,
      'center' => NULL,
      'vertical_tabs' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8c77ed0c-5f88-4485-9a1f-ea41342bbbab';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_cod_visa_letters_page__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bc2c24de-7164-4255-b93b-488c89a00aea';
  $pane->panel = 'center';
  $pane->type = 'cod_visa_letter_entity_form_pane';
  $pane->subtype = 'cod_visa_letter_entity_form_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'entity_type' => 'cod_visa_letter',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bc2c24de-7164-4255-b93b-488c89a00aea';
  $display->content['new-bc2c24de-7164-4255-b93b-488c89a00aea'] = $pane;
  $display->panels['center'][0] = 'new-bc2c24de-7164-4255-b93b-488c89a00aea';
  $pane = new stdClass();
  $pane->pid = 'new-670ecd76-0440-437a-bb11-61fc55892f86';
  $pane->panel = 'vertical_tabs';
  $pane->type = 'cod_visa_letters_vertical_tabs_pane';
  $pane->subtype = 'cod_visa_letters_vertical_tabs_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '670ecd76-0440-437a-bb11-61fc55892f86';
  $display->content['new-670ecd76-0440-437a-bb11-61fc55892f86'] = $pane;
  $display->panels['vertical_tabs'][0] = 'new-670ecd76-0440-437a-bb11-61fc55892f86';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['cod_visa_letters_page'] = $page;

  return $pages;

}
