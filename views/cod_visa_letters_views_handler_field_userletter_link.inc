<?php

/**
 * @file
 * Definition of cod_visa_letters_views_handler_field_userletter_link.
 */

/**
 * Field handler to present a link to registration cancel.
 *
 * @ingroup views_field_handlers
 */
class cod_visa_letters_views_handler_field_userletter_link extends ticket_views_handler_field_registration_link {

  function render_link($data, $values) {
    $ticket_registration = entity_load_single('ticket_registration', $data);
    $entity_type = $bundle = 'cod_visa_letter';
    $config = cod_events_get_eck_template($ticket_registration->host(), $entity_type, $bundle);

    // Found a config! load it.
    if (!empty($config) && $ticket_registration->state == 'completed') {
      $wrapper = entity_metadata_wrapper('cod_visa_letter', $config);
      $webform = $wrapper->field_webform_node->value();

      // Webform found, grab the specific entry for this ticket.
      if (!empty($webform)) {
        $trid_cid = webform_get_cid($webform, 'trid', 0);
        $sid = db_select('webform_submitted_data', 'wsd')
          ->fields('wsd', array('sid'))
          ->condition('cid', $trid_cid)
          ->condition('nid', $webform->nid)
          ->condition('data', $data)
          ->execute()
          ->fetchField();
        if ($sid) {
          $text = !empty($text) ? $text : t('Visa Letter');
          $link = l($text, "ticket_registration/$data/visa-letter");
          return $link;
        }
      }
    }
  }
}
