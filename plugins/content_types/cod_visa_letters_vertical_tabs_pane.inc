<?php
/**
 * This plugin array is more or less self documenting
 */
$plugin = array(
  'title' => t('Visa Letter Vertical Tabs'),
  'single' => TRUE,
  'category' => array(t('Conference Organizing'), -9),
  'render callback' => 'cod_visa_letters_vertical_tabs_pane_render'
);

function cod_visa_letters_vertical_tabs_pane_render($subtype, $conf, $context = NULL) {
  $block = new stdClass();
  $block->title = '';

  $conference = array_shift($context);
  // TODO: Don't hardcode this.
  $links = array(
    l('Visa Letter Template', 'admin/conference/'.$conference.'/tickets/visa-letters/cod_visa_letter'),
    l('Visa Letters', 'admin/conference/'.$conference.'/tickets/visa-letters'),
  );

  $entity_type = $bundle = 'cod_visa_letter';
  // Check to see if there is a webform already referenced
  $config = cod_events_get_eck_template($conference, $entity_type, $bundle);

  // Found a config! load it.
  if (!empty($config)) {
    $wrapper = entity_metadata_wrapper('cod_visa_letter', $config);
    $webform = $wrapper->field_webform_node->value();

    if(!empty($webform)) {
      array_unshift($links, l('Visa Webform', 'node/'.$webform->nid.'/edit'));
    }
  }

  $output = '<ul class="vertical-tabs-list">';
  $first = TRUE;
  $last = FALSE;
  $count = 0;
  foreach ($links as $link) {
    $count++;
    if ($count == sizeof($links)) {
      $last = TRUE;
    }
    $classes = 'vertical-tab-button';
    $first ? $classes .= ' first ' : '';
    $last ? $classes .= ' last' : '';
    $output .= '<li tabindex="-1" class="'.$classes.'">'.$link.'</li>';
    $first = FALSE;
  }
  $output .= '</ul>';

  $block->content = $output;
  return $block;
}
