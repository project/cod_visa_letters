<?php

/**
 * Implements hook_views_data_alter().
 */
function cod_visa_letters_views_data_alter(&$data) {
  // Display the visa letter link.
  $data['webform_submissions']['cod_visa_letter_link'] = array(
    'title' => t('Visa Letter Link'),
    'help' => t('Provides a downloadable pdf link.'),
    'real field' => 'serial',
    'field' => array(
      'handler' => 'cod_visa_letters_views_handler_field_letter_link',
      'click sortable' => TRUE,
      'real field' => 'serial',
      'link_type' => 'visa_letter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['ticket_registration']['cod_visa_letter_link'] = array(
    'field' => array(
      'title' => t('Visa Letter Link'),
      'help' => t('Provides a downloadable pdf link.'),
      'handler' => 'cod_visa_letters_views_handler_field_userletter_link',
    ),
  );
}