<?php

/**
 * @file
 *
 * Sample relationship plugin.
 *
 * We take a simplecontext, look in it for what we need to make a relcontext, and make it.
 * In the real world, this might be getting a taxonomy id from a node, for example.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Webform from cod_visa_letter"),
  'keyword' => 'cod_visa_letter_webform',
  'description' => t('Adds the visa webform from existing conference node.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'cod_visa_letter_webform_context_from_node_context',
  'settings form' => 'cod_visa_letter_webform_context_from_node_context_settings_form',
);

/**
 * Return a new context based on an existing context.
 */
function cod_visa_letter_webform_context_from_node_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:node', NULL);
  }

  // Check to see if there is a webform already referenced
  $entity_type = $bundle = 'cod_visa_letter';
  $config = cod_events_get_eck_template($context->data, $entity_type, $bundle);

  // Found a config! load it.
  if (!empty($config)) {
    $wrapper = entity_metadata_wrapper('cod_visa_letter', $config);
    $context->data = $wrapper->field_webform_node->value();
  }
  // Create the new context from some element of the parent context.
  // In this case, we'll pass in the whole context so it can be used to
  // create the relcontext.
  return ctools_context_create('cod_visa_letter_webform', $context);
}

/**
 * Settings form for the relationship.
 */
function cod_visa_letter_webform_context_from_node_context_settings_form($conf) {
  // We won't configure it in this case.
  return array();
}

