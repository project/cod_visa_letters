<?php
/**
 * @file
 * cod_visa_letters.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cod_visa_letters_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function cod_visa_letters_eck_bundle_info() {
  $items = array(
    'cod_visa_letter_cod_visa_letter' => array(
      'machine_name' => 'cod_visa_letter_cod_visa_letter',
      'entity_type' => 'cod_visa_letter',
      'name' => 'cod_visa_letter',
      'label' => 'cod_visa_letter',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function cod_visa_letters_eck_entity_type_info() {
  $items = array(
    'cod_visa_letter' => array(
      'name' => 'cod_visa_letter',
      'label' => 'cod_visa_letter',
      'properties' => array(
        'language' => array(
          'label' => 'Entity language',
          'type' => 'language',
          'behavior' => 'language',
        ),
      ),
    ),
  );
  return $items;
}
