<?php

/**
 * @file
 * Definition of cod_sponsors_commerce_views_handler_field_order_link.
 */

/**
 * Field handler to present a visa letter link to the order.
 *
 * @ingroup views_field_handlers
 */
class cod_visa_letters_views_handler_field_letter_link extends webform_handler_field_submission_link {

  /**
   * Load the node and submissions needed for this components values.
   */
  function pre_render(&$values) {
    // Load all the submissions needed for this page. This is stored at the
    // view level to ensure it's available between fields so we don't load
    // them twice.
    foreach ($values as $key => $value) {
      $sid = $value->sid;
      if (!isset($webform) || (isset($webform) && $webform->nid != $value->webform_submissions_nid)) {
        $webform = node_load($value->webform_submissions_nid);
      }
      $trid_cid = webform_get_cid($webform, 'trid', 0);
      $trid = db_select('webform_submitted_data', 'wsd')
        ->fields('wsd', array('data'))
        ->condition('cid', $trid_cid)
        ->condition('nid', $webform->nid)
        ->condition('sid', $sid)
        ->execute()
        ->fetchField();
      if (!empty($trid)) {
        $values[$key]->trid = $trid;
      }
    }
  }

  function render($values) {
    // Cannot print out a pdf if we don't have a trid.
    if(!isset($values->trid)) {
      return;
    }
    $trid = $values->trid;
    $serial = $values->{$this->aliases['serial']};
    $is_draft = $values->{$this->aliases['is_draft']};

    // Get the trid from the submission


    $text = str_ireplace('[serial]',
                          $serial . ($is_draft ? ' (' . t('draft') . ')' : ''),
                          $this->options['text']);
    switch ($this->link_type) {
      case 'visa_letter':
        $text = !empty($text) ? $text : t('Visa Letter');
        $link = l($text, "ticket_registration/$trid/visa-letter");
        break;
      default:
        return;
    }

    /*
    if ($this->options['access_check']) {
      module_load_include('inc', 'webform', 'includes/webform.submissions');
      $node = node_load($nid);
      $submission = webform_get_submission($nid, $sid);
      if (!webform_submission_access($node, $submission, $this->link_type)) {
        return;
      }
    }
    */

    return $link;
  }
}
